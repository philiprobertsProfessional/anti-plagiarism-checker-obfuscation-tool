Anti Plagiarism Checker Obfuscation Tool


Converts paragraphs of words into synonyms that don't trigger plagiarism checkers.

Written in python

**Python libraries Dependencies:**

```
* bs4
* requests
```

Install these libraries with:

`pip install requests bs4`

**How to use:**

Paste text into a file named "data.txt" in the same directory as main.py. Run the main file with

`python main.py`

and wait until it finishes executing. The output is now in the file output.txt.











